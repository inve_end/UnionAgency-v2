### 创建上分订单
URL: console/recharge\_order/make_order/ <br/>
METHOD: POST <br/>
PARAMS: <br/>

| name | type | required | desc |
| -------- | -------- | -------- | -------- |
| mch_id | int | y | 商户id |
| user_id | int | y | 用户id |
| pay_type | enum | y | bankcard \| alipay \| wechat |
| amount | float | y | 充值金额 |
| receive\_bankcard\_id | int | n | 收款账户id |
| pay\_account\_username | string | n | 转账人姓名 |
| pay\_account\_num | string | n | 转账人账号，银行卡，支付宝，微信账号 |
| pay\_account\_bank | string | n | 转账银行 |

银行列表：<br/>
工商银行 <br/>
建设银行 <br/>
中国银行 <br/>
农业银行 <br/>
交通银行 <br/>
招商银行 <br/>
中信银行 <br/>
光大银行 <br/>
华夏银行 <br/>
浦发银行 <br/>
兴业银行 <br/>
兴业银行 <br/>
民生银行 <br/>
平安银行 <br/>
广发银行 <br/>
邮政银行 <br/>
中邮政银行 <br/>
哈尔滨银行 <br/>
浙商银行 <br/>
青岛银行 <br/>
长沙银行 <br/>
南京银行 <br/>
其它银行 <br/>


RESPONSE: <br/>

```javascript
{
  "order_id": 1,
  "amount": 12.23,
  "bankcard_info": {
      "bank": "中国银行",
      "account_number": "52259494838993",
      "subbranch": "中关付支行",
      "account_holder": "刘欢"
  }}
```


### 获取后台上分时可使用的银行卡列表
URL: console/bankcard/list/chargeable/\<mch_id\>/ <br/>
METHOD: get <br/>
PARAMS: <br/>

RESPONSE: <br/>

```javascript
{
  "bankcards": [
    "id": 1,
    "name": "DP01BC0115",
    "bank": "工商银行",
    "account_holder": "赵三儿",
    "account_number": "6222034001237123185"
  ]
}
```
