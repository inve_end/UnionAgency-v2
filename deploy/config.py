PROJECT_DIRS = {
    'fin': '/your-code/UnionAgency-V2/finservice',
    'fin-web': '/your-code/UnionAgency-Console/dist',
}

SETTING_FILES_PATH = {
    'fin': 'base',
}

STAGES = {
    "prod": {
        "roledefs": {
            'fin': ['103.230.240.228:22202', ],
            'fin-web': ['103.230.240.228:22202', ],
        },
        "user": "ubuntu",
        "key_filename": "~/.ssh/id_rsa",
    },
    "release": {
        "roledefs": {
            'fin': ['219.135.56.195:22202', ],
            'fin-web': ['103.230.240.50:22202', ],
        },
        "user": "ubuntu",
        "key_filename": "~/.ssh/id_rsa",
    },
}
