# -*- coding: utf-8 -*-
import os
import sys
from time import sleep

import arrow
from fabric.api import sudo, env, run
from fabric.colors import red
from fabric.contrib.project import rsync_project

from config import STAGES, SETTING_FILES_PATH
from console import print_start, print_done, print_error

# fab -f fabfile.py -R fin --set stage=release deploy

if "stage" not in env:
    print(red(u" ✘   Stage not set, use `--set stage=<STAGE>`.\n"))
    sys.exit()

if env.stage not in STAGES:
    print(red(u" ✘   Stage invalid, check `config.py`.\n"))
    sys.exit()
env.roledefs = STAGES[env.stage]["roledefs"]
env.setting_file_path = SETTING_FILES_PATH
env.user = STAGES[env.stage]["user"]
env.key_filename = STAGES[env.stage]["key_filename"]

_ROLE = env.roles[0]
_PROJECT = _ROLE.lstrip('witch-').lstrip('loki-')
_REMOTE_PROJECT_DIR = "/opt/%s" % _PROJECT
_AVAILABLE_DIR = "%s/available/" % _REMOTE_PROJECT_DIR
_ENABLED_DIR = "%s/enabled/" % _REMOTE_PROJECT_DIR
_ENABLED_APP_DIR = "%s/enabled/app" % _REMOTE_PROJECT_DIR
_SETTING_DIR = "%s/enabled/app/%s/" % (_REMOTE_PROJECT_DIR, env.setting_file_path.get(_ROLE))
_LOG_DIR = "/var/log/%s" % _PROJECT

_CWD = os.getcwd()
_SOURCE_CODE_DIR = os.path.join(os.path.dirname(_CWD), 'finservice')


def upload_src():
    print_start("Uploading source code")
    now_str = arrow.now().format("YYYYMMDDHHmmss")
    available_app_dir = "%s/available/app_%s/" % (_REMOTE_PROJECT_DIR, now_str)
    run("mkdir -p %s" % available_app_dir)
    rsync_project(available_app_dir, "%s/" % _SOURCE_CODE_DIR,
                  exclude=['.git', '*.tar.bz2', '.idea', '*.deb', '.gitignore', '.python-version', '*.pyc'],
                  extra_opts="--filter='dir-merge,- .gitignore'")
    run("rm -fr %s" % _ENABLED_APP_DIR)
    run("ln -s %s %s" % (available_app_dir, _ENABLED_APP_DIR))
    if _ROLE in SETTING_FILES_PATH:
        run("mv %ssettings_%s.py %s/env_settings.py" % (_SETTING_DIR, env.stage, _SETTING_DIR))
    print_done("Uploading source code done.")


def restart_service():
    """
    Sometime program is running, but supervisor lose track of its pid. Then the program go wildly.
    You have to kill the program, then start the program using `supervisorctl start <NAME>`.
    Really sucks!
    """
    if '-web' in _ROLE:
        return
    print_start("restarting %s" % _ROLE)
    sudo("supervisorctl update")
    sudo("supervisorctl stop %s:fin_uwsgi" % _PROJECT)
    sudo("supervisorctl stop %s:fin_processor" % _PROJECT)
    sleep(5)
    sudo("chown ubuntu:ubuntu -R %s" % _LOG_DIR)
    sudo("supervisorctl start %s:fin_uwsgi" % _PROJECT)
    sudo("supervisorctl start %s:fin_processor" % _PROJECT)
    print_done("restarting %s done." % _ROLE)


def deploy():
    print_start("Starting deploy")
    if len(env.roles) != 1:
        print_error("Roles invalid!")
    upload_src()
    restart_service()
    print_done("Deploy done.\n")
