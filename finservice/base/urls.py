from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    '',
    url(r'^fin/api/', include('api.urls')),
    url(r'^api/v2/', include('api.urls_v2')),
    #url(r'^fin/admin/', include('console.urls')),
    url(r'^fin/console/', include('console.urls')),
)
