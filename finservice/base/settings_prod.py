# coding=utf-8
DEBUG = False

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://bigbang:n0ra@wang@103.230.243.167:3306/financial?charset=utf8',
    'DEBUG': DEBUG
}

MYSQL_SLAVE_CONF = {
    'db': 'mysql://bigbang:n0ra@wang@103.59.41.76:3306/financial?charset=utf8',
    'DEBUG': DEBUG
}

CONSOLE_CONF = {
    'db': 'mysql://finadmin:P@55word@103.230.243.167:3306/console_admin?charset=utf8',
    'DEBUG': DEBUG
}

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

CELERY_BROKER = 'redis://127.0.0.1:6379//'

# Only Used in V1 Charge
PAY_GATEWAY_URL = {
    'loki': 'http://www.luckycp365.com/api/v1/get_unionagent_page/%s',
    'witch': 'http://api.91whcp.com:9001/api/v1/get_unionagent_page/%s'
}

PAY_FILL_URL = {
    'loki': "http://www.luckycp365.com/api/v1/fill_unionagent_charge",
    'witch': "http://api.91whcp.com:9001/api/v1/fill_unionagent_charge",
}

APP_OPEN_URL = 'xingyuncp://open/home?index=0'

QIJI_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/qiji_withdraw/'
# MIIDOW 修改回调需联系对方
#MIIDOW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/miidow_withdraw'
RECHARGE_URL = 'http://api.maiunion.net:9003/api/v2/recharge/create/'

HYDRA_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/hydra/'
HYDRA_WITHDRAW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/hydra_withdraw/'
ONEPAY_WITHDRAW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/onepay_withdraw/{}/{}'
KYJHPAY_WITHDRAW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/kyjhpay_withdraw/'
FLASHPAY_WITHDRAW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/flashpay_withdraw/'

NOTIFY_HYDRA_BANKCARDS = 'http://103.71.51.51:5006/api/Cards/UACards/Update'

GROUP_WITHDRAW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/group_withdraw/'

MEIFUBAO_WITHDRAW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/meifubao_withdraw/'
