# coding=utf-8
DEBUG = False

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://fin:n0ra@wang@127.0.0.1:3306/financial_test?charset=utf8',
    'DEBUG': DEBUG
}

MYSQL_SLAVE_CONF = MYSQL_CONF

CONSOLE_CONF = {
    'db': 'mysql://fin:n0ra@wang@127.0.0.1:3306/financial_admin?charset=utf8',
    'DEBUG': DEBUG
}

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

CELERY_BROKER = 'redis://127.0.0.1:6379//'

NOTIFY_HYDRA_BANKCARDS = 'http://103.230.240.107:5006/api/Cards/UACards/Update'

# 本机ip
NOTIFY_PREFIX = 'http://219.135.56.195:8084'

PAY_CACHE_URL = 'http://219.135.56.195:8084/pay/api/cache/'

PAY_GATEWAY_URL = {
    'loki': 'http://www.luckycp365.com/api/v1/get_unionagent_page/%s',
    'witch': 'http://13.250.105.3/api/v1/get_unionagent_page/%s'
}
#PAY_GATEWAY_URL = 'http://14.29.47.193:9003/fin/api/gateway/%s'

PAY_FILL_URL = {
    'loki': "http://www.luckycp365.com/api/v1/fill_unionagent_charge",
    'witch': "http://13.250.105.3/api/v1/fill_unionagent_charge",
}
#PAY_FILL_URL = 'http://14.29.47.193:9003/fin/api/charge/fill'

APP_OPEN_URL = 'xingyuncp://open/home?index=0'

QIJI_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/qiji_withdraw/'
# MIIDOW 修改回调需联系对方
MIIDOW_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/miidow_withdraw'
RECHARGE_URL = 'http://test.maiunion.net:8084/api/v2/recharge/create/'

HYDRA_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/hydra/'
HYDRA_WITHDRAW_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/hydra_withdraw/'
ONEPAY_WITHDRAW_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/onepay_withdraw/{}/{}'
KYJHPAY_WITHDRAW_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/kyjhpay_withdraw/'
FLASHPAY_WITHDRAW_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/flashpay_withdraw/'

MEIFUBAO_WITHDRAW_NOTIFY_URL = 'http://219.135.56.195:8084/fin/api/notify/meifubao_withdraw/'