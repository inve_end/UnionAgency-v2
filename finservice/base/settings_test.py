# coding=utf-8
DEBUG = False
#DEBUG = True

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://test:123456@127.0.0.1:3306/financial?charset=utf8',
    'DEBUG': DEBUG
}

MYSQL_SLAVE_CONF = {
    'db': 'mysql://test:123456@127.0.0.1:3306/financial?charset=utf8',
    'DEBUG': DEBUG
}

CONSOLE_CONF = {
    'db': 'mysql://test:123456@127.0.0.1:3306/console_admin?charset=utf8',
    'DEBUG': DEBUG
}

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

CELERY_BROKER = 'redis://127.0.0.1:6379//'

PAY_GATEWAY_URL = {
    'loki': 'http://192.168.20.173/api/v1/get_unionagent_page/%s',
    'witch': 'http://192.168.20.173/api/v1/get_unionagent_page/%s',
}
PAY_FILL_URL = {
    'witch': "http://192.168.20.173/fin/api/charge/fill",
    'loki': "http://192.168.20.173/fin/api/charge/fill",
}

APP_OPEN_URL = 'xingyuncp://open/home?index=0'
QIJI_NOTIFY_URL = 'http://dev.maiunion.net:9003/fin/api/notify/qiji_withdraw/'
# MIIDOW 修改回调需联系对方
#MIIDOW_NOTIFY_URL = 'http://api.maiunion.net:9003/fin/api/notify/miidow_withdraw'
RECHARGE_URL = 'http://test.maiunion.net:8084/api/v2/recharge/create/'
