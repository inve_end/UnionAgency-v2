# -*- coding: utf-8 -*-
from __future__ import absolute_import
import logging

from async.celery import app
from common.recharge import db as recharge_db
from common.withdraw import db as withdraw_db
from common.mch import handler as mch_handler
from common.timer import handler as timer_handler
from common.timer import TIMER_EVENT_TYPE

_LOGGER = logging.getLogger('worker')


@app.task(name='common.notify_mch')
def notify_mch(order_id):
    ''' 上分通知 '''
    _LOGGER.info('to process async job notify_mch, order[%s]', order_id)
    order = recharge_db.get_order(order_id)
    success, order = mch_handler.notify_mch(order)
    if not success:
        # start timer to notify again
        timer_handler.submit_notify_event(order, TIMER_EVENT_TYPE.MCH_NOTIFY)
    return success, order


@app.task(name='common.notify_mch_withdraw')
def notify_mch_withdraw(order_id):
    ''' 下分通知 '''
    _LOGGER.info('to process async job notify_mch_withdraw, order[%s]', order_id)
    order = withdraw_db.get_order(order_id)
    success, order = mch_handler.notify_mch_withdraw(order)
    if not success:
        # start timer to notify again
        timer_handler.submit_notify_event(order, TIMER_EVENT_TYPE.MCH_WITHDRAW_NOTIFY)
    return success, order
