# -*- coding: utf-8 -*-
import requests

HOST = 'http://localhost:8000/fin/console/'
HEADERS = {
    'X-AUTH-TOKEN': '1796b6d6-6165-40b1-a501-d22be7dc6f76',
    'X-AUTH-USER': 48,
}


def test_chargeable_api():
    res = requests.get(HOST + 'bankcard/list/chargeable/3/', headers=HEADERS)
    print("========> ", res.text)


def test_create_order_by_console():
    data = {
        'mch_id': 1,
        'user_id': 183497,
        'pay_type': 'wechat',
        'amount': 12.11,
        'receive_bankcard_id': 61,
        'pay_account_username': 'll002',
        # 'pay_account_bank': '中国银行',
        'pay_account_num': 'llll@173.com',

    }
    res = requests.post(HOST + 'recharge_order/make_order/', json=data, headers=HEADERS)
    print("========> ", res.text)


if __name__ == '__main__':
    # test_chargeable_api()
    test_create_order_by_console()
