import unittest
import json
from django.utils.encoding import smart_unicode
from common.order_inquiry.model  import OrderInquiry
from common.order_inquiry import db as inqdb
from common.order_inquiry.model import *
from common.recharge import db as recharge_db
from common.recharge.model import *

req = '{"order_inquiry_no": "201903214200005","mch_id": 5, "status":2}'
#  "out_trade_no": "Order00008",
#  "user_id": 1,
#  "username": "test_user",
#  "amount": 10,
#  "transaction_time": "2019-03-16 15:32:00", 
#  "receipt": {"receipt": ["https://www.dropbox.com/s/mn5smjgwtlbndqu/dropbox-dropdown.png?raw=1", "https://www.dropbox.com/s/mn5smjgwtlbndqu/dropbox-dropdown.png?raw=1"]},
#  "notify_url": "http://219.135.56.195:8084/fin/api/notify/hydra_withdraw_test/Durotar",
#  "sign": "16c742b1ecf66a155d7b51fbe861b2c4"
#}

order_inquiry_id = '6'
 

class OrderInquiryTestCase(unittest.TestCase):  

    def test_order_inquiry_no(self):
        self.assertIsNotNone(order_inquiry_id)

    def test_get_order_inquiry_by_id(self):
        order_inquiry = inqdb.get_orderinq(6)
        self.assertIsNotNone(order_inquiry)

    def test_json_request(self):
        query_dct = json.loads(smart_unicode(req))
        self.assertIsNotNone(query_dct)
    
    def test_recharge_order(self):
        order_inquiry = inqdb.get_orderinq(6)
        order = recharge_db.get_order_by_out_trade_no(order_inquiry.mch_id, order_inquiry.out_trade_no)
        self.assertIsNotNone(order.order_inquiry_status)

    def test_recharge_inquiry_status(self):        
        order_inquiry = inqdb.get_orderinq(6)
        order = recharge_db.get_order_by_out_trade_no(order_inquiry.mch_id, order_inquiry.out_trade_no)        
        self.assertNotEqual(order.order_inquiry_status, INQUIRY_STATUS.FAILED)
        self.assertNotEqual(order.order_inquiry_status, INQUIRY_STATUS.SUCCESS)
        #new_order = recharge_db.upsert_order(order_param, order.id)

    def test_updated_inquiry_status(self):
       query_dct = json.loads(smart_unicode(req))
       order_inquiry = inqdb.get_orderinq(6)

       order = recharge_db.get_order_by_out_trade_no(order_inquiry.mch_id, order_inquiry.out_trade_no)
       
       if order.order_inquiry_status not in [INQUIRY_STATUS.SUCCESS, INQUIRY_STATUS.FAILED]:
           new_order  = recharge_db.update_order_inquiry_status(order.id, query_dct['status'])
           self.assertIsNotNone(new_order)