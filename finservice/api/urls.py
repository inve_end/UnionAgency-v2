# coding=utf-8
from api.charge import view as charge
from api.third import view as third
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    # 支付相关
    url(r'charge/create/?$', charge.create),
    url(r'charge/query/?$', charge.query),
    url(r'charge/fill/?$', charge.fill),

    url(r'cache/(?P<cache_id>[^/]+)/?$', charge.get_cache_page),
    url(r'gateway/(?P<order_id>[^/]+)/?$', charge.get_gateway_page),
    # 第三方回调接口
    url(r'notify/third_qa/?$', third.notify_qa),
    url(r'notify/qiji_withdraw/(?P<app_id>[^/]+)/?$', third.qiji_notify),
    url(r'notify/miidow_withdraw/?$', third.miidow_notify),
    url(r'notify/tonglue_withdraw/?$', third.tonglue_withdraw_notify),
    url(r'notify/hydra/(?P<app_id>[^/]+)/?$', third.hydra_notify),
    url(r'notify/hydra_withdraw/(?P<app_id>[^/]+)/?$', third.hydra_withdraw_notify),
    url(r'notify/group_withdraw/(?P<app_id>[^/]+)/?$', third.group_withdraw_notify),
    url(r'notify/meifubao_withdraw/(?P<app_id>[^/]+)/?$', third.meifubao_withdraw_notify),
    url(r'notify/onepay_withdraw/(?P<app_id>[^/]+)/(?P<order_id>[^/]+)?$', third.onepay_withdraw_notify),
    url(r'notify/kyjhpay_withdraw/(?P<app_id>[^/]+)/?$', third.kyjhpay_withdraw_notify),
    url(r'notify/xinglongpay_withdraw/?$', third.xinglongpay_withdraw_notify),
    url(r'notify/flashpay_withdraw/(?P<app_id>[^/]+)/?$', third.flashpay_withdraw_notify),
)
