# coding=utf-8
from api.withdraw import view as withdraw
from api.recharge import view as recharge
from api.third import view as third
from console.views import bankcard
from console.views.order_inquiry import OrderInquiryView, SingleOrderInquiryView, create_order_inquiry, query as order_inquiry_query 
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    # 下分V2接口
    url(r'withdraw/create/?$', withdraw.create),
    url(r'withdraw/query/?$', withdraw.query),

    # 上分V2接口
    url(r'recharge/create/?$', recharge.create),
    url(r'recharge/query/?$', recharge.query),

    # 获取自有可用银行卡列表
    url(r'recharge/get_our_bankcards/?$', bankcard.list_our_bankcard),
    
    url(r'orderinquiry/create/?$', create_order_inquiry),
    url(r'orderinquiry/query/?$', order_inquiry_query)
)
