#! -*- coding:utf-8 -*-

from django.core.management.base import BaseCommand

from common.third.shuangqian_withdraw import query_order
from common.utils import track_logging
from common.withdraw.model import WithdrawOrder, WITHDRAW_ORDER_STATUS

_LOGGER = track_logging.getLogger('withdraw')


class Command(BaseCommand):

    def handle(self, **kwargs):
        channel_id = 14
        items = WithdrawOrder.query.filter(WithdrawOrder.third_type == channel_id). \
            filter(WithdrawOrder.status == WITHDRAW_ORDER_STATUS.THIRD_UNKNOWN).all()
        for item in items:
            query_order(202488, item.id)
