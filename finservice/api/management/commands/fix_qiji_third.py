#! -*- coding:utf-8 -*-
import logging
from django.core.management.base import BaseCommand
import csv
from common.mch import handler as mch_handler
from common.withdraw.model import WithdrawOrder, WITHDRAW_ORDER_STATUS
_LOGGER = logging.getLogger('worker')



# */5 * * * * cd /opt/fin/enabled/app/;/home/ubuntu/.pyenv/versions/bigbang/bin/python manage.py expire_timeout_order
class Command(BaseCommand):

    def handle(self, **kwargs):
        print '------begin------'
        with open('/home/kael/qiji_qew.csv', 'r') as f:
            reader = csv.reader(f)
            head_row = next(reader)
            count = 0
            for row in reader:
                count += 1
                print row[0]
                order_id = row[0].replace('\xc2\xa0', '')
                item = WithdrawOrder.query.filter(WithdrawOrder.id == int(order_id)).\
                    filter(WithdrawOrder.status == WITHDRAW_ORDER_STATUS.THIRD).first()
                if item:
                    print('order %s fixed' % order_id)
                    item.status = WITHDRAW_ORDER_STATUS.DONE
                    item.extra_info = u'奇迹下分 三方未回调修复'
                    item.save()
                    mch_handler.notify_mch_withdraw(item)
        print '------done------'
