#! -*- coding:utf-8 -*-
import logging
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand

from common.withdraw.model import WithdrawOrder, WITHDRAW_ORDER_STATUS
from common.withdraw.handler import process_withdraw

_LOGGER = logging.getLogger('worker')


# */5 * * * * cd /opt/fin/enabled/app/;/home/ubuntu/.pyenv/versions/bigbang/bin/python manage.py expire_timeout_order
class Command(BaseCommand):

    def handle(self, **options):
        start = datetime(year=2018, month=10, day=1)
        print 'Run fix withdraw'
        count = 0
        withdraw_orders = WithdrawOrder.query.filter(WithdrawOrder.created_at >= start). \
            filter(WithdrawOrder.status == WITHDRAW_ORDER_STATUS.THIRD_UNKNOWN).all()
        for order in withdraw_orders:
            process_withdraw(order)
            count += 1
            print count
        print 'end fix withdraw'
