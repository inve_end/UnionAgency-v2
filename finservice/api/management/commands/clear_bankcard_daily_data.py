#! -*- coding:utf-8 -*-

from django.core.management.base import BaseCommand

from common.bankcard.model import BankcardData
from common.utils import track_logging

_LOGGER = track_logging.getLogger('worker')

# cd /opt/fin/enabled/app;/home/ubuntu/.pyenv/versions/bigbang/bin/python manage.py clear_bankcard_daily_data


class Command(BaseCommand):

    def handle(self, **kwargs):
        card_data = BankcardData.query.filter().all()
        for item in card_data:
            item.daily_recharge_count = 0
            item.daily_recharge_total = 0
            item.daily_withdraw_count = 0
            item.daily_withdraw_total = 0
            item.save()
