#! -*- coding:utf-8 -*-
import logging
from django.core.management.base import BaseCommand

from common.recharge import db as recharge_db
from common.third.tonglue import tonglue_revoke_order

_LOGGER = logging.getLogger('worker')



# */5 * * * * cd /opt/fin/enabled/app/;/home/ubuntu/.pyenv/versions/bigbang/bin/python manage.py expire_timeout_order
class Command(BaseCommand):

    def handle(self, **options):
        print 'Run ExpireTimeoutOrder'
        _LOGGER.info('Run ExpireTimeoutOrder')
        try:
            order_ids, third_ids = recharge_db.expire_timeout_order()
            _LOGGER.info('ExpireTimeoutOrder success')
            for third_id in third_ids:
                if third_id:
                    tonglue_revoke_order(third_id)
        except Exception as e:
            _LOGGER.exception('CardBalanceHandler process error.(%s)' % e)
        print 'end ExpireTimeoutOrder'
