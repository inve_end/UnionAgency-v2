# -*- coding: utf-8 -*-
import time

from common.cache import ProxyAgent, prefix_key
from common.utils import id_generator
from common.utils import track_logging
from common.utils.decorator import cache_wrapper

_LOGGER = track_logging.getLogger(__name__)
_LOCK_TIMEOUT = 10
_ACCOUNT_EXPIRE = 3600


@cache_wrapper
def submit_timer_event(event_type, cache_value, timestamp):
    """
    used for db
    """
    key = prefix_key('timerzset:%s' % event_type)
    return ProxyAgent().zadd(key, timestamp, cache_value)


@cache_wrapper
def range_expired_events(event_type, max_time):
    """
    used for db
    """
    key = prefix_key('timerzset:%s' % event_type)
    return ProxyAgent().zrangebyscore(key, 0, max_time)


@cache_wrapper
def remove_expired_event(event_type, event_value):
    """
    used for db
    """
    key = prefix_key('timerzset:%s' % event_type)
    return ProxyAgent().zrem(key, event_value)


@cache_wrapper
def timer_event_processed(event_id):
    """
    used for db
    """
    key = prefix_key('timerlock:%s' % event_id)
    return not ProxyAgent().setnx(key, int(time.time()))


@cache_wrapper
def save_html(pay_id, html_text):
    cache_id = id_generator.generate_uuid('{}{}'.format(pay_id, int(time.time())))
    key = prefix_key('chargehtml:%s' % cache_id)
    ProxyAgent().setex(key, 300, html_text)
    return cache_id


@cache_wrapper
def get_cache_html(cache_id):
    key = prefix_key('chargehtml:%s' % cache_id)
    return ProxyAgent().get(key)
