# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum


CHASE_TYPE = Enum({
    "PLUS": (0L, "加分"),
    "MINUS": (1L, "减分")
})

CHASE_ORIGIN_TYPE = Enum({
    "RECHARGE": (0L, "充值订单"),
    "WITHDRAW": (1L, "提现订单"),
})

CHASE_ORDER_STATUS = Enum({
    "CREATED": (0L, "已提交"),
    "PROCESSED": (1L, "已处理"),
    "FAIL": (2L, "失败"),
})


class ChaseOrder(orm.Model):
    __tablename__ = 'chase_order'
    id = orm.Column(orm.BigInteger, primary_key=True)
    mch_id = orm.Column(orm.SmallInteger, default=0)
    amount = orm.Column(orm.Integer)
    status = orm.Column(orm.SmallInteger)
    chase_type = orm.Column(orm.SmallInteger, default=0)
    chase_origin_type = orm.Column(orm.SmallInteger, default=0)
    chase_origin_id = orm.Column(orm.BigInteger, default=0)
    user_id = orm.Column(orm.VARCHAR)
    admin = orm.Column(orm.VARCHAR)
    desc = orm.Column(orm.TEXT)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
