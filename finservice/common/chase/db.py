# -*- coding: utf-8 -*-

from sqlalchemy import func

from common.chase.model import *
from common.utils import track_logging
from common.utils.db import list_object
from common.utils.db import paginate
from common.utils.decorator import sql_wrapper

_LOGGER = track_logging.getLogger(__name__)


@sql_wrapper
def create_order(params):
    chase_order = ChaseOrder()
    for k, v in params.iteritems():
        setattr(chase_order, k, v)
    chase_order.status = CHASE_ORDER_STATUS.CREATED  # 暂无处理流程，直接成功
    chase_order.save()
    return chase_order


@sql_wrapper
def get_order(order_id):
    return ChaseOrder.query.filter(ChaseOrder.id == order_id).first()


@sql_wrapper
def list_order(query_dct):
    query, total_count = list_object(query_dct, ChaseOrder, disable_paginate=True)
    total_amount = query.statement.with_only_columns([func.sum(ChaseOrder.amount)])
    total_amount = query.session.execute(total_amount).scalar()
    query = paginate(query, query_dct)
    return query.all(), total_count, total_amount


@sql_wrapper
def update_order(order_id, status):
    order = ChaseOrder.query.filter(ChaseOrder.id == order_id).first()
    order.status = status
    order.save()
