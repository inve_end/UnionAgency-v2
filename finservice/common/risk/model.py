# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

ACTION_TYPE = Enum({
    "RECHARGE": (0L, u"充值订单"),
    "WITHDRAW": (1L, u"提现订单"),
})

RISK_STATUS = Enum({
    "DISABLED": (0L, u"禁用"),
    "AVAILABLE": (1L, u"可用"),
})


class RiskyUser(orm.Model):
    __tablename__ = "risky_user"
    id = orm.Column(orm.BigInteger, primary_key=True)
    mch_id = orm.Column(orm.VARCHAR)
    user_id = orm.Column(orm.Integer)
    star = orm.Column(orm.SmallInteger)
    client_ip = orm.Column(orm.VARCHAR)
    region = orm.Column(orm.VARCHAR)
    register_day = orm.Column(orm.SmallInteger)
    recharge_avg = orm.Column(orm.BigInteger)
    recharge_total = orm.Column(orm.BigInteger)
    recharge_req_count = orm.Column(orm.BigInteger)
    recharge_succ_count = orm.Column(orm.BigInteger)
    recharge_fail_count = orm.Column(orm.BigInteger)
    recharge_succ_rate = orm.Column(orm.Integer)
    withdraw_total = orm.Column(orm.BigInteger)
    withdraw_succ_count = orm.Column(orm.BigInteger)
    withdraw_fail_count = orm.Column(orm.BigInteger)
    withdraw_req_count = orm.Column(orm.BigInteger)
    desc = orm.Column(orm.TEXT)
    recharge_status = orm.Column(orm.SmallInteger, default=1)
    withdraw_status = orm.Column(orm.SmallInteger, default=1)
    admin = orm.Column(orm.SmallInteger)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class RiskyCard(orm.Model):
    __tablename__ = "risky_card"
    id = orm.Column(orm.BigInteger, primary_key=True)
    card_num = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.VARCHAR)
    status = orm.Column(orm.SmallInteger)
    desc = orm.Column(orm.TEXT)
    admin = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class RiskyIP(orm.Model):
    __tablename__ = "risky_ip"
    id = orm.Column(orm.BigInteger, primary_key=True)
    risk_user_id = orm.Column(orm.BigInteger)
    action_type = orm.Column(orm.BigInteger)
    ip = orm.Column(orm.VARCHAR)
    region = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
