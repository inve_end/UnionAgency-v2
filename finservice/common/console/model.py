# -*- coding: utf-8 -*-
from django.conf import settings

from common.utils.types import Enum
from common.utils.orm import ArmoryOrm


orm = ArmoryOrm()
orm.init_conf(settings.CONSOLE_CONF)

FORBIDDEN_ROLE = 0
USER_NOT_DELETED = 0
USER_DELETED = 1
USER_DISABLE = 0
USER_ENABLE = 1

PERMISSION = Enum({
    "NONE": (0, "none"),
    "READ": (1, "read"),
    "WRITE": (2, "write"),
})

PERMISSION_MOD = Enum({
    "USER": (1L, "用户登陆"),
    "CONSOLE": (2L, "控制台管理"),
    "MCH": (3L, "平台管理"),
    "WITHDRAW_CHANNEL": (4L, "提现通道管理"),
    "RISK": (5L, "风控管理"),
    "BANKCARD": (7L, "银行卡管理"),
    "PAY_ORDER": (8L, "订单管理"),
    "DATA": (9L, "数据统计"),
    "ORDER_INQUIRY": (10L, "订单申诉")
})

ACTION = {
    'POST': 1,
    'PUT': 2,
    'PATCH': 2,
    'DELETE': 3
}

RESOURCE = {}

class User(orm.Model):
    __tablename__ = 'user'
    id = orm.Column(orm.BigInteger, primary_key=True)
    nickname = orm.Column(orm.VARCHAR)
    username = orm.Column(orm.VARCHAR)
    password = orm.Column(orm.VARCHAR)
    role = orm.Column(orm.SmallInteger)
    enabled = orm.Column(orm.SmallInteger)
    deleted = orm.Column(orm.SmallInteger)
    use_safe_dog = orm.Column(orm.SmallInteger, default=1)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class Role(orm.Model):
    __tablename__ = 'role'
    id = orm.Column(orm.BigInteger, primary_key=True)
    rolename = orm.Column(orm.VARCHAR)
    permissions = orm.Column(orm.VARCHAR)
    mch_ids = orm.Column(orm.VARCHAR)
    enabled = orm.Column(orm.SmallInteger)
    deleted = orm.Column(orm.SmallInteger)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class Permission(orm.Model):
    __tablename__ = 'permission'
    id = orm.Column(orm.BigInteger, primary_key=True)
    url = orm.Column(orm.VARCHAR)
    action = orm.Column(orm.VARCHAR)
    permission_mod = orm.Column(orm.VARCHAR)
    desc = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class UserToken(orm.Model):
    __tablename__ = 'user_token'
    user_id = orm.Column(orm.BigInteger, primary_key=True)
    token = orm.Column(orm.VARCHAR, primary_key=True)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class Record(orm.Model):
    __tablename__ = 'record'
    id = orm.Column(orm.BigInteger, primary_key=True)
    resource = orm.Column(orm.VARCHAR)
    resource_id = orm.Column(orm.BigInteger)
    action = orm.Column(orm.SmallInteger)
    content = orm.Column(orm.Text)
    operator = orm.Column(orm.BigInteger)
    created_at = orm.Column(orm.DATETIME)
