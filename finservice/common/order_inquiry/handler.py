# -*- coding: utf-8 -*-
import json
import requests

from common.order_inquiry import db as inqdb
from common.order_inquiry.model import INQUIRY_STATUS, NOTIFY_STATUS, REASON_FAILURE
from common.utils import exceptions as err
from common.utils import track_logging
from common.utils import tz
from common.mch import handler as mch_handler

_LOGGER = track_logging.getLogger('fin')

ORDER_INQUIRY_FINISHED = lambda status: status in [INQUIRY_STATUS.SUCCESS, INQUIRY_STATUS.FAILED]
ORDER_INQUIRY_NOT_FINISHED = lambda status: status not in [INQUIRY_STATUS.SUCCESS, INQUIRY_STATUS.FAILED]


def get_order_inquiry_by_mch(mch_id, inquiry_no):
    order_inquiry = inqdb.get_order_inquiry_by_mch(mch_id, inquiry_no)
    fail_reason = ''
    if order_inquiry.reason_failure_id:
        fail_reason = REASON_FAILURE.get_label(order_inquiry.reason_failure_id).decode('utf-8')
    return {
        'order_inquiry_no': order_inquiry.order_inquiry_no,
        'out_trade_no': order_inquiry.out_trade_no,
        'amount': order_inquiry.amount,
        'status_id': order_inquiry.status,
        'status': INQUIRY_STATUS.get_key(order_inquiry.status),
        'reason_failure_id': order_inquiry.reason_failure_id,
        'reason_failure': fail_reason,
        'updated_at': tz.utc_to_local_str(order_inquiry.updated_at) if order_inquiry.updated_at else None,
    }


def get_order_inquiry_notify(inquiry_no):
    order_inquiry = inqdb.get_order_inquiry(inquiry_no)
    fail_reason = None
    if order_inquiry.reason_failure_id:
        fail_reason = REASON_FAILURE.get_label(order_inquiry.reason_failure_id).decode('utf-8')
    return {
        'order_inquiry_no': order_inquiry.order_inquiry_no,
        'out_trade_no': order_inquiry.out_trade_no,
        'amount': float(order_inquiry.amount),
        'status_id': order_inquiry.status,
        'status': INQUIRY_STATUS.get_key(order_inquiry.status),
        'reason_failure_id': order_inquiry.reason_failure_id,
        'reason_failure': fail_reason,
        'updated_at': tz.utc_to_local_str(order_inquiry.updated_at) if order_inquiry.updated_at else None,
    }


def notify(order_inquiry):
    if ORDER_INQUIRY_NOT_FINISHED(order_inquiry.status):
        _LOGGER.info('notify order_inquiry_id {} notify failed order_inquiry.status is {}'.format(order_inquiry.id,
                                                                                                  INQUIRY_STATUS.get_key(
                                                                                                      order_inquiry.status)))
        return

    notify_data = get_order_inquiry_notify(order_inquiry.order_inquiry_no)

    calculated_sign = mch_handler.generate_sign(order_inquiry.mch_id, notify_data)
    notify_data['sign'] = calculated_sign

    response = requests.post(order_inquiry.notify_url, data=json.dumps(notify_data),
                             headers={'Content-Type': 'application/json'})
    response_text = response.text.upper() if response.status_code == 200 else 'FAILED'
    status = NOTIFY_STATUS.get_value(response_text)
    if not status:
        raise err.ResourceNotFound('notify response {} not found'.format(response.text.upper()))
    return status


def check_valid_order(order_inquiry, status_func=None, msg=None):
    if order_inquiry and status_func and status_func(order_inquiry.status):
        error = '{} {}'.format(msg, INQUIRY_STATUS.get_key(
            order_inquiry.status)) if msg else 'order_id {} is already {}'.format(
            order_inquiry.out_trade_no, INQUIRY_STATUS.get_key(order_inquiry.status))
        raise err.DataError(error)
    return True


def json_validator(data):
    try:
        json.loads(data)
        return True, None
    except ValueError as error:
        return False, "invalid json: %s" % error


def check_receipt(data):
    receipt = json.loads(data)
    if receipt:
        if len(receipt) == 0:
            raise err.DataError('Min receipt is one. Received no receipt.')
