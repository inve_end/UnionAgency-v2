# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

INQUIRY_STATUS = Enum({
    "PENDING": (0L, u"待处理"),
    "PROCESSING": (1L, u"处理中"),
    "SUCCESS": (2L, u"成功"),
    "FAILED": (3L, u"失败"),
})

NOTIFY_STATUS = Enum({
    "READY": (0L, "ready to notify"),
    "FAILED": (1L, "notify failed"),
    "SUCCESS": (2L, "notify success"),
})
 

REASON_FAILURE = Enum({
    "REPEAT_PAYMENT": (0L, "重复支付"),
    "MODIFICATION_AMOUNT": (1L, "修改金额"),
    "PRIVATE_TRANSFER": (2L, "私自转账"),
    "CHECK_NO_ORDER": (3L, "查无订单"),
    "NOT_EXISTING": (4L, "姓名不符"),
    "INSUFFICIENT_CREDENTIALS": (5L, "凭证不足"),
    "OVERTIME": (6L, "超时"),
    "INCORRECT_CERT": (7L, "凭证有误请查看示例"),
    "VERIFY_CHECK_WITHOUT_ORDER": (8L, "查无订单请重新核实"),
    "ORDER_TIMEOUT": (9L, "订单超时需重新提交"),
    "ERROR_REFILL": (10L, "发起错误充值订单申诉"),
    "SPECIAL_CONTACT_CS": (11L, "特殊情况请咨询客服"),
    "UPLOAD_FAIL": (12L, "凭证上传失败请重新申诉"),
    "NAME_UNMATCHED": (13L, "姓名不符请重新确认"),
    "VOUCHER_VAGUE": (14L, "凭证模糊请重新申诉"),
    "CANNOT_APPEAL": (15L, "不可申诉")
})

class OrderInquiry(orm.Model):
    __tablename__ = 'order_inquiry'
    id = orm.Column(orm.BigInteger, primary_key=True)
    order_inquiry_no = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.Integer)
    out_trade_no = orm.Column(orm.VARCHAR)
    service = orm.Column(orm.VARCHAR)
    user_id = orm.Column(orm.BigInteger)
    username = orm.Column(orm.VARCHAR)
    amount = orm.Column(orm.Integer)
    transaction_time = orm.Column(orm.DATETIME)
    processing_time = orm.Column(orm.DATETIME)
    status = orm.Column(orm.Integer)
    notify_url = orm.Column(orm.VARCHAR)
    mch_create_ip = orm.Column(orm.VARCHAR)
    sign = orm.Column(orm.VARCHAR)
    receipt = orm.Column(orm.VARCHAR)
    reason_failure_id = orm.Column(orm.Integer)
    operator = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    status_update_time = orm.Column(orm.DATETIME)
    notified_at = orm.Column(orm.DATETIME)
    notify_status = orm.Column(orm.Integer)
    notify_count = orm.Column(orm.Integer)