# -*- coding: utf-8 -*-
import json

from future.utils import raise_with_traceback

from common.cache import redis_cache
from common.utils import id_generator
from common.utils import track_logging
from common.utils.exceptions import DataError
from common.utils.types import Enum

_LOGGER = track_logging.getLogger(__name__)

TIMER_EVENT_TYPE = Enum({
    "MCH_NOTIFY": (1L, "notify mch in time"),
    "MCH_WITHDRAW_NOTIFY": (2L, "notify mch withdraw in time"),
    "FETCH_TONGLUE_BALANCE": (3L, "get bankcard real balance from tonglue"),
    "CHECK_BANKCARD_LIMIT": (5L, "check & change the limit status of bankcards"),
})


class TimerEvent(object):
    """Timer event wrapper
    """

    def __init__(self, event_type, event_value, timestamp):
        self.event_type = event_type
        # must be a json dict, like {'id': xxxx, 'msg': xxxx}
        self.event_value = event_value
        self.timestamp = timestamp

    @classmethod
    def submit(cls, event_type, event_msg, timestamp):
        # construct event_value dict
        event_value = {}
        uuid = id_generator.generate_uuid()
        event_value.update({'id': uuid})
        event_value.update({'msg': event_msg})
        try:
            cache_value = json.dumps(event_value)
            redis_cache.submit_timer_event(event_type, cache_value, timestamp)
        except Exception as e:
            _LOGGER.error('timer event submit error.(%s)' % e)
            raise_with_traceback(DataError(e))

        return uuid


class EventHandler(object):
    """
    Abstract class
    """

    def process(self, event):
        raise NotImplementedError(
            'EventHandler class is supposed to an abstract class')
