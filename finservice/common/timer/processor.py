# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
import sys

# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(
    os.path.dirname(os.path.dirname(__file__)))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'

import time
import json
import logging
from multiprocessing import Process
from common.cache import redis_cache

from common.timer import TIMER_EVENT_TYPE, handler, routine_task_handler
from common.utils.tz import now_ts
from common.utils.pid_file import PIDFile

from django.conf import settings
import logging.config

logging.config.dictConfig(settings.LOGGING)
_LOGGER = logging.getLogger('worker')

DEFAULT_EVENT_HANDLERS = {
    TIMER_EVENT_TYPE.MCH_NOTIFY: handler.MchNotifyHandler(),
    TIMER_EVENT_TYPE.MCH_WITHDRAW_NOTIFY: handler.MchNotifyWithdrawHandler(),
    TIMER_EVENT_TYPE.FETCH_TONGLUE_BALANCE: routine_task_handler.CardBalanceHandler(),
    TIMER_EVENT_TYPE.CHECK_BANKCARD_LIMIT: routine_task_handler.CheckBankcardLimit(),
}


_PROCESS_TIMEOUT = 30


class EventProcessor(object):
    """
    timer event processor
    """

    def __init__(self, event_type_list):
        self.event_type_list = event_type_list
        self.event_handlers = DEFAULT_EVENT_HANDLERS
        self.infinite_process = True
        self.sleep_second = 0.5

    def get_expired_events(self, event_type):
        event_list = []
        try:
            max_time = now_ts()
            value_list = redis_cache.range_expired_events(event_type, max_time)
            for event_value in value_list:
                event_list.append(event_value)
        except Exception as e:
            _LOGGER.exception('get_expired_events failed.(%s)' % e)

        return event_list

    def process(self, event_type):
        _LOGGER.info("begin processing timer event: %s" %
                     TIMER_EVENT_TYPE.get_label(event_type))
        event_handler = self.event_handlers.get(event_type)
        if not event_handler:
            _LOGGER.error('event handler not found for %s(%s)',
                          event_type, TIMER_EVENT_TYPE.get_label(event_type))
            return

        while self.infinite_process:
            events = self.get_expired_events(event_type)
            for event_value in events:
                _LOGGER.info('start event: %s' % event_value)
                try:
                    event_msg = json.loads(event_value)

                    def handler_process(event_id):
                        success = event_handler.process(event_msg['msg'])
                        # remove timer key
                        redis_cache.remove_expired_event(
                            event_type, event_value)
                        if not success:
                            next_exec_time = now_ts() + 10
                            redis_cache.submit_timer_event(
                                event_type, event_value, next_exec_time)

                    handler_process(event_msg['id'])
                except Exception as e:
                    _LOGGER.error('event value invalid.(%s:%s) %s' %
                                  (event_type, event_value, e))

            if not events:
                time.sleep(self.sleep_second)

    def run(self):
        _LOGGER.info("timer processor run...")
        p_list = []
        for event_type in self.event_type_list:
            p = Process(target=self.process, args=(event_type,))
            p_list.append(p)
            p.start()

        for p in p_list:
            p.join()


if __name__ == "__main__":
    PIDFile('/tmp/fin_process.pid')
    event_type_list = [k for k in TIMER_EVENT_TYPE.to_dict()]
    event_processor = EventProcessor(event_type_list)
    event_processor.run()
