# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum


TRANS_TYPE = Enum({
    "RECHARGE": (1L, "收款"),
    "WIHTDRAW": (2L, "付款"),
    "MANUAL": (3L, "人工填报"),
})


TRANS_TARGET_TYPE = Enum({
    "INTERNAL_OUT": (1L, "内部转出"),
    "EXTERNAL_IN": (2L, "外部转入"),
    "EXTERNAL_OUT": (3L, "外部转出"),
})


class Transaction(orm.Model):
    __tablename__ = 'transaction'
    id = orm.Column(orm.BigInteger, primary_key=True)
    trans_type = orm.Column(orm.SmallInteger)
    card_id = orm.Column(orm.BigInteger)
    card_name = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.SmallInteger, default=0)
    amount = orm.Column(orm.Float)
    addition_amount = orm.Column(orm.Float)
    target_type = orm.Column(orm.SmallInteger)
    target_card_id = orm.Column(orm.BigInteger)
    target_card_name = orm.Column(orm.VARCHAR)
    rollbacked = orm.Column(orm.SmallInteger) # 1: 回滚过 2: 未回滚
    admin = orm.Column(orm.VARCHAR)
    desc = orm.Column(orm.TEXT)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
