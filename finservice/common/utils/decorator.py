# -*- coding: utf-8 -*-
import json
from functools import partial

from django.conf import settings
from django.utils.encoding import smart_unicode
from future.utils import raise_with_traceback
from pymongo.errors import PyMongoError
from redis import RedisError
from sqlalchemy.exc import SQLAlchemyError

from common import orm, slave
from common.console.db import get_user
from common.utils import JsonResponse
from common.utils import track_logging
from common.utils.exceptions import (Error, DbError,
                                     CacheError, GCodeError)
from common.utils.respcode import StatusCodeDict
from common.utils.safedog import google_verify_token

_LOGGER = track_logging.getLogger(__name__)


def _wrap2json(data):
    if data is None:
        data = {}
    elif isinstance(data, dict) or isinstance(data, list):
        return JsonResponse(dict(status=0, msg='', data=data), status=200)
    else:
        return data


def response_wrapper(func):
    def _wrapper(request, *args, **kwargs):

        if settings.DEBUG:
            return _wrap2json(func(request, *args, **kwargs))

        try:
            return _wrap2json(func(request, *args, **kwargs))
        except Error as e:
            _LOGGER.info('server error! %s %s %s', e.HTTPCODE, unicode(e) or StatusCodeDict.get(e.STATUS),
                         request.path)
            return JsonResponse(
                dict(status=e.STATUS,
                     msg=unicode(e) or StatusCodeDict.get(e.STATUS)),
                status=e.HTTPCODE)
        except:
            user_repr = "%s (%s)" % (request.user.id, request.user.username) if request.user else ''
            _LOGGER.exception('REQUEST URL: %s, USER: %s' % (request.path, user_repr))
            return JsonResponse(
                dict(status=Error.STATUS, msg=u'未知错误'),
                status=Error.HTTPCODE)

    return _wrapper


def common_sql_wrapper(orm):
    def handler(func):
        def _wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                orm.session.rollback()
                raise e
            finally:
                orm.session.close()

        return _wrapper

    return handler


sql_wrapper = partial(common_sql_wrapper(orm=orm))
slave_wrapper = partial(common_sql_wrapper(orm=slave))


def cache_wrapper(func):
    def _wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except RedisError as e:
            raise_with_traceback(CacheError(e))
        except Error:
            raise
        except Exception as e:
            raise_with_traceback(Error(e))

    return _wrapper


def mongo_wrapper(func):
    def _wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except PyMongoError as e:
            raise_with_traceback(DbError(e))
        except Error:
            raise
        except Exception as e:
            raise_with_traceback(Error(e))

    return _wrapper


def _check_safe_code(req):
    _LOGGER.info('check_safe_code in : %s', req.body)
    info = json.loads(smart_unicode(req.body))
    g_code = info.get('g_code', '')
    g_amount = int(info.get('g_amount', 0))
    if req.path.startswith('/fin/console'):
        if get_user(req.user_id).use_safe_dog and g_amount >= 1000:
            if not google_verify_token(req.user_id, g_code, '127.0.0.1'):
                raise GCodeError()


def safe_wrapper(func):
    def _wrapped(request, *args, **kwargs):
        _check_safe_code(request)
        return func(request, *args, **kwargs)

    return _wrapped
