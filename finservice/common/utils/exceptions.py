# -*- coding: utf-8 -*-
from common.utils.respcode import HttpCode, StatusCode


# Basic Exceptions
class Error(Exception):
    HTTPCODE = HttpCode.SERVER_ERROR
    STATUS = StatusCode.UNKNOWN_ERROR

    def __init__(self, msg='', httpcode=None, status=None):
        super(Error, self).__init__(msg)
        if httpcode:
            self.HTTPCODE = httpcode
        if status:
            self.STATUS = status


class ClientError(Error):
    HTTPCODE = HttpCode.BAD_REQUEST


class ServerError(Error):
    pass


class ThirdPartyError(Error):
    pass


class WechatError(ThirdPartyError):
    pass


class PingXXError(ThirdPartyError):
    STATUS = StatusCode.PINGXX_PLATFORM_ERROR

# General Exceptions


class ParamError(ClientError):
    STATUS = StatusCode.PARAM_REQUIRED


class DataError(ClientError):
    STATUS = StatusCode.DATA_ERROR


class DbError(ServerError):
    STATUS = StatusCode.DB_ERROR


class DataConflict(ClientError):
    STATUS = StatusCode.DATA_CONFLICT


class CacheError(ServerError):
    STATUS = StatusCode.CACHE_ERROR


# Specific Exception
class ProtocolError(ClientError):
    HTTPCODE = HttpCode.FORBIDDEN
    STATUS = StatusCode.HTTPS_REQUIRED


class SmsPlatformError(ServerError):
    STATUS = StatusCode.SMS_PLATFORM_ERROR


class AuthenticateError(DataError):
    STATUS = StatusCode.INVALID_TOKEN
    HTTPCODE = HttpCode.UNAUTHORIZED


class PermissionError(ClientError):
    STATUS = StatusCode.NOT_ALLOWED
    HTTPCODE = HttpCode.FORBIDDEN


class NotImplementedError(ServerError):
    HTTPCODE = HttpCode.NOT_IMPLEMENTED


class ResourceInsufficient(ClientError):
    HTTPCODE = HttpCode.FORBIDDEN
    STATUS = StatusCode.RESOURCE_INSUFFICIENT


class ResourceNotFound(ClientError):
    HTTPCODE = HttpCode.NOT_FOUND


class ResourceNotModified(ClientError):
    HTTPCODE = HttpCode.NOT_MODIFIED


class BalanceInsufficient(ClientError):
    STATUS = StatusCode.BALANCE_INSUFFICIENT


class TermExpired(ClientError):
    STATUS = StatusCode.TERM_EXPIRED


class ReachLimit(ClientError):
    STATUS = StatusCode.REACH_LIMIT


class GCodeError(ClientError):
    STATUS = StatusCode.GCODE_ERROR
