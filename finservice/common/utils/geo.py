# -*- coding: utf-8 -*-
import geoip2

from django.conf import settings

DEFAULT_COUNTRY = ""

city_reader = None


def get_city(ip, lan='zh-CN'):
    try:
        global city_reader
        if city_reader is None:
            city_reader = geoip2.database.Reader(settings.GEOLITE_CITY_DB)
        city_obj = city_reader.city(ip)
        city_name = city_obj.city.names.get(lan)
        if not city_name:
            city_name = city_obj.subdivisions[0].names.get(lan)
        return city_name
    except:
        return DEFAULT_COUNTRY
