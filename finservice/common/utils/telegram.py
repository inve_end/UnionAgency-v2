# -*- coding: utf-8 -*-
import json
import requests
from functools import partial

# _SEND_TEXT_API_TELE = 'https://api.telegram.org/bot673458136:AAEO26ohSgBMMm5DYKPxmdAkTvBdGF6nVqc/sendMessage'
_SEND_TEXT_API_TELE = 'https://api.telegram.org/bot689081328:AAGbiyP5vGeISo-dYg_ALG1k3Mu3OSvoOOw/sendMessage'
# https://api.telegram.org/bot689081328:AAGbiyP5vGeISo-dYg_ALG1k3Mu3OSvoOOw/getupdates


def send_text_msg_tele(chat_text):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    post_data = {
        "chat_id": '-1001331494801',
        "text": chat_text,
        "parse_mode": "Markdown",
    }
    try:
        response = requests.post(_SEND_TEXT_API_TELE, \
            data=json.dumps(post_data, separators=(',', ':')), headers=headers, timeout=5)
    except:
        pass
    return True


if __name__ == "__main__":
    # d = {
    #     "text": "\u4e0b\u5206\u901a\u9053\uff1a**4**-**\u4e1c\u839e\u5e02\u73af\u6cfd\u7535\u5b50\u79d1\u6280\u6709\u9650\u516c\u53f8** \u62a5\u9519\nPAYEE_NOT_EXIST\n\u8ba2\u5355\u53f7\uff1a%s\n",
    #     "parse_mode": "Markdown", "chat_id": -220589678}
    order_msg = u'\n订单号：%s\n' % 12345678
    send_text_msg_tele('Witch 收款卡BB123达到限额:\n 使用时间达到限额')
