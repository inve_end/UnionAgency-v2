# -*- coding: utf-8 -*-
import json

import requests
from django.conf import settings

from common.utils import track_logging

_LOGGER = track_logging.getLogger(__name__)


def tonglue_get_balance(card_number, bank_flag):
    try:
        post_data = {
            "apikey": settings.THIRD_API_KEY,
            "bank_flag": bank_flag,
            "card_number": card_number,
        }

        response = requests.post(settings.THIRD_URL + 'authority/system/api/query_bankcard/',
                                 headers={'Content-type': 'application/json', 'Accept': 'text/plain'},
                                 data=json.dumps(post_data), timeout=3)
        response = json.loads(response.content)
        # _LOGGER.info('got bankcard %s balance: %s' % (card_number, response))
        if response[u'success']:
            return response[u'balance']
        else:
            return -1
    except Exception, e:
        _LOGGER.error('got bankcard %s balance fail: %s' % (card_number, e))
        return -1


if __name__ == "__main__":
    tonglue_check_balance('CMB', '6214831223428027')
