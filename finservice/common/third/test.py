# -*- coding: utf-8 -*-

from common.utils import track_logging
from common.third.onepay_withdraw import create_order
from common.withdraw.admin_db import get_withdraw_channel
from common.withdraw.db import get_order

_LOGGER = track_logging.getLogger('withdraw')


def test_create_onepay_withdraw(order_id=395190, channel_id=40):
    channel = get_withdraw_channel(channel_id)
    order = get_order(order_id)
    return create_order(order, channel)
