# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

BANK_CARD_STATUS = Enum({
    "UNAVAILABLE": (0L, "已关闭"),
    "AVAILABLE": (1L, "已开启"),
    "DEACTIVATED": (2L, "已停用")
})

BANK_CARD_TYPE = Enum({
    "TEST": (1L, "测试卡"),
    "RECHARGE": (2L, "上分卡"),
    "WITHDRAW": (3L, "下分卡"),
    "ORECHARGE": (4L, "HYDRA上分卡"),
    "BILLCARD": (5L, "结算卡"),
    "TRANSCARD": (6L, "中转卡"),
    "AGENT_RECHARGE": (7L, "代理上分卡"),
})

TRANS_TYPE = Enum({
    "RECHARGE": (1L, "玩家上分"),
    "WITHDRAW": (2L, "玩家下分"),
    "TRANS_IN": (3L, "人工转入"),
    "TRANS_OUT": (4L, "人工转出"),
    "SERIVCE_FEE": (5L, "手续费支出"),
    "OTHER": (6L, "其它类型"),
})

BANK_DCT = {
    u'工商银行': 'ICBC',
    u'中国工商银行': 'ICBC',
    u'建设银行': 'CCB',
    u'中国建设银行': 'CCB',
    u'中国银行': 'BOC',
    u'农业银行': 'ABC',
    u'中国农业银行': 'ABC',
    u'交通银行': 'BCM',
    u'中国交通银行': 'BCM',
    u'招商银行': 'CMB',
    u'中国招商银行': 'CMB',
    u'中信银行': 'CNCB',
    u'中国中信银行': 'CNCB',
    u'光大银行': 'CEB',
    u'中国光大银行': 'CEB',
    u'华夏银行': 'HXB',
    u'中国华夏银行': 'HXB',
    u'浦发银行': 'SPDB',
    u'上海浦东发展银行': 'SPDB',
    u'中国浦发银行': 'SPDB',
    u'兴业银行': 'CIB',
    u'中国兴业银行': 'CIB',
    u'民生银行': 'CMBC',
    u'中国民生银行': 'CMBC',
    u'平安银行': 'PAB',
    u'中国平安银行': 'PAB',
    u'广发银行': 'CGB',
    u'中国广发银行': 'CGB',
    u'邮政银行': 'PSBC',
    u'中邮政银行': 'PSBC',
    u'中国邮政储蓄银行': 'PSBC',
    u'哈尔滨银行': 'HRBCB',
    u'浙商银行': 'CZB',
    u'青岛银行': 'QDCCB',
    u'长沙银行': 'CSCB',
    u'南京银行': 'NJCB',
    u'其它银行': 'THIRD_NOT_SUPPORT'
}

BANKCARD_IMG = {
    u'工商银行': 'http://p.hcoriental.com/log_bank_gongs.png',
    u'建设银行': 'http://p.hcoriental.com/log_bank_jians.png',
    u'中国银行': 'http://p.hcoriental.com/log_bank_zhongg.png',
    u'农业银行': 'http://p.hcoriental.com/log_bank_longy.png',
    u'交通银行': 'http://p.hcoriental.com/log_bank_jiaot.png',
    u'招商银行': 'http://p.hcoriental.com/log_bank_zhaos.png',
    u'中信银行': 'http://p.hcoriental.com/log_bank_zhongx.png',
    u'光大银行': 'http://p.hcoriental.com/log_bank_guangd.png',
    u'华夏银行': 'http://p.hcoriental.com/log_bank_huax.png',
    u'浦发银行': 'http://p.hcoriental.com/log_bank_puf.png',
    u'兴业银行': 'http://p.hcoriental.com/log_bank_xingy.png',
    u'民生银行': 'http://p.hcoriental.com/log_bank_mins.png',
    u'平安银行': 'http://p.hcoriental.com/log_bank_pinga.png',
    u'广发银行': 'http://p.hcoriental.com/log_bank_guangf.png',
    u'邮政银行': 'http://p.hcoriental.com/log_bank_youz.png',
    u'哈尔滨银行': 'http://p.hcoriental.com/log_bank_hae.png',
    u'其它银行': 'THIRD_NOT_SUPPORT'
}

STRATEGY_IMG = 'http://p.hcoriental.com/strategy.jpg'

MAXIMUM_STAR = 6

STRATEGY_IMG = 'http://p.hcoriental.com/strategy.jpg'

STAR_STRATEGY = {
    # account_day, recharge, bet, recharge_count, withdraw_count, ratio
    1: [0, 0, 0, 1, 0, 0],
    2: [10, 1000, 1500, 10, 0, 1.5],
    3: [20, 10000, 15000, 20, 10, 1.5],
    4: [40, 100000, 200000, 40, 20, 2],
    5: [80, 500000, 1000000, 80, 40, 2],
    6: [160, 1000000, 2000000, 160, 80, 2]
}


class Bankcard(orm.Model):
    __tablename__ = 'bankcard_list'
    id = orm.Column(orm.BigInteger, primary_key=True)
    card_type = orm.Column(orm.SmallInteger)
    name = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.Integer)
    account_holder = orm.Column(orm.VARCHAR)
    account_number = orm.Column(orm.VARCHAR)
    bank = orm.Column(orm.VARCHAR)
    region = orm.Column(orm.Integer)
    status = orm.Column(orm.SmallInteger)
    phone = orm.Column(orm.VARCHAR)
    email = orm.Column(orm.VARCHAR)
    usheild_date = orm.Column(orm.VARCHAR)
    subbranch = orm.Column(orm.VARCHAR)
    account = orm.Column(orm.VARCHAR)
    password = orm.Column(orm.VARCHAR)
    calculate_balance = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    real_balance = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    desc = orm.Column(orm.Text)
    reached_daily_limit = orm.Column(orm.SmallInteger)
    reached_limit = orm.Column(orm.SmallInteger)
    deleted = orm.Column(orm.SmallInteger)  # 废弃
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    reminder_sent_at = orm.Column(orm.DATETIME)


class BankcardData(orm.Model):
    __tablename__ = 'bankcard_data'
    card_id = orm.Column(orm.BigInteger, primary_key=True)
    name = orm.Column(orm.VARCHAR)
    recharge_count = orm.Column(orm.BigInteger)
    withdraw_count = orm.Column(orm.BigInteger)
    daily_recharge_count = orm.Column(orm.BigInteger)
    daily_withdraw_count = orm.Column(orm.BigInteger)
    recharge_total = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    withdraw_total = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    daily_recharge_total = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    daily_withdraw_total = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class BankcardStar(orm.Model):
    __tablename__ = 'bankcard_stars'
    id = orm.Column(orm.BigInteger, primary_key=True)
    card_id = orm.Column(orm.BigInteger)
    star = orm.Column(orm.BigInteger)
    created_at = orm.Column(orm.DATETIME)


class BankcardPeroid(orm.Model):
    __tablename__ = 'bankcard_peroid'
    id = orm.Column(orm.BigInteger, primary_key=True)
    card_type = orm.Column(orm.VARCHAR)
    bank = orm.Column(orm.VARCHAR)
    daily_max_recharge = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    daily_max_recharge_count = orm.Column(orm.BigInteger)
    daily_max_withdraw = orm.Column(orm.Float)  # 实际为DECIMAL(12,2)
    daily_max_withdraw_count = orm.Column(orm.BigInteger)
    max_recharge = orm.Column(orm.BigInteger)  # 实际为DECIMAL(14,2)
    max_withdraw = orm.Column(orm.BigInteger)  # 实际为DECIMAL(14,2)
    max_day = orm.Column(orm.BigInteger)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
