# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

PAY_TYPE = Enum({
    "BANKCARD": ('bankcard', "银行卡"),
    "ALIPAY": ('alipay', "支付宝"),
    "WECHAT": ('wechat', "微信")
})

RECHARGE_ORDER_STATUS = Enum({
    "CREATED": (-1, u"已创建未FILL"),
    "WAIT": (0, u"待上分"),
    "DONE": (1, u"已上分"),
    "REJECTED": (2, u"拒绝上分"),
    "THIRD": (4, u"提交第三方"),
    "OVERTIME": (8, u"超时"),
})

# 此状态只在api返回时有，数据库无
RECHARGE_ORDER_SUMMARY = Enum({
    "CREATED": (1, u"未完成"),
    "DONE": (2, u"成功"),
    "REJECTED": (3, u"拒绝"),
})

RECHARGE_NOTIFY_STATUS = Enum({
    "READY": (0L, "ready to notify"),
    "FAIL": (1L, "notify failed"),
    "SUCC": (2L, "notify successed"),
})

# 超过30min 订单超时
EXPIRE_TIME = 2700

# Send reminder after 30 mins
REMINDER_TIME = 1800


class RechargeOrder(orm.Model):
    __tablename__ = 'recharge_order'
    id = orm.Column(orm.BigInteger, primary_key=True)
    mch_id = orm.Column(orm.Integer)
    user_id = orm.Column(orm.BigInteger)
    pay_type = orm.Column(orm.VARCHAR)
    pay_account_bank = orm.Column(orm.VARCHAR)
    pay_account_num = orm.Column(orm.VARCHAR)
    pay_account_username = orm.Column(orm.VARCHAR)
    context = orm.Column(orm.TEXT)
    receive_card_num = orm.Column(orm.VARCHAR)
    receive_card_name = orm.Column(orm.VARCHAR)
    amount = orm.Column(orm.Integer)
    region = orm.Column(orm.VARCHAR)
    sdk_version = orm.Column(orm.VARCHAR)
    out_trade_no = orm.Column(orm.VARCHAR)
    star = orm.Column(orm.SmallInteger)
    status = orm.Column(orm.VARCHAR)
    notify_url = orm.Column(orm.VARCHAR)
    notify_status = orm.Column(orm.SmallInteger)
    notify_count = orm.Column(orm.SmallInteger)
    notify_at = orm.Column(orm.DATETIME)
    order_inquiry_status = orm.Column(orm.SmallInteger)
    third_id = orm.Column(orm.VARCHAR)
    detail = orm.Column(orm.TEXT)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    payed_at = orm.Column(orm.DATETIME)
    receive_card_type = orm.Column(orm.Integer)
    order_creater = orm.Column(orm.VARCHAR)
