# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

WITHDRAW_CHANNEL_NOT_DELETED = 0
WITHDRAW_CHANNEL_DELETED = 1

WITHDRAW_TYPE = Enum({
    "BANKCARD": ('bankcard', "银行卡"),
    "ALIPAY": ('alipay', "支付宝"),
    "WECHAT": ('wechat', "微信")
})

WITHDRAW_ORDER_STATUS = Enum({
    "WAIT": (0L, u"待下分"),
    "LOCKED": (1L, u"已锁定"),
    "DONE": (2L, u"已下分"),
    "REJECTED": (3L, u"拒绝下分"),
    "THIRD": (4L, u"已提交第三方"),
    "THIRD_FAIL": (5L, u"第三方失败"),
    "THIRD_UNKNOWN": (6L, u"第三方状态未知"),
})

WITHDRAW_NOTIFY_STATUS = Enum({
    "READY": (0L, "ready to notify"),
    "FAIL": (1L, "notify failed"),
    "SUCC": (2L, "notify successed"),
})

CHANNEL_TYPE = Enum({
    "MANUAL": (0L, u"人工代付"),
    "QIJI": (1L, u"奇迹代付"),
    "MIIDOW": (2L, u"米多代付"),
    "TONGLUE_WITHDRAW": (3L, u"同略云自有卡"),
    "HYDRA_WITHDRAW": (4L, u"HYDRA自有卡"),
    "GROUP_WITHDRAW": (5L, u"GROUP代付"),
    "SHUANGGIAN_WITHDRAW": (6L, u"双乾网络代付"),
    "MEIFUBAO_WITHDRAW": (7L, u"美付宝代付"),
    "ONEPAY_WITHDRAW": (8L, u"易付"),
    "KYJHPAY_WITHDRAW": (9L, u"快一聚合代付"),
    "XINGLONGPAY_WITHDRAW": (10L, u"兴隆代付"),
    "FLASHPAY_WITHDRAW": (11L, u"flash代付"),
})

WITHDRAW_THIRD_TYPE = Enum({
    "MANUAL": (0L, u"人工代付"),
    "QIJI": (1L, u"奇迹代付"),
    "MIIDOW": (2L, u"MIIDOW代付-itech1"),
    "QIJI_WITCH": (3L, u"奇迹代付-100109"),
    "QIJI_LOKI": (4L, u"奇迹代付-100106"),
    "QIJI_DUROTAR": (5L, u"奇迹代付-100107"),
    "QIJI_ZS": (6L, u"奇迹代付-100110"),
    "MIIDOW2": (7L, u"MIIDOW代付-itech2"),
})

WITHDRAW_CHANNEL_STATUS = Enum({
    "DISABLED": (0L, "已关闭"),
    "ENABLED": (1L, "已开启"),
    "LIMITED": (2L, "达到限额"),
})

WITHDRAW_BATCH_TYPE = Enum({
    "LOCK": (0L, u"锁定"),
    "REPEAT": (1L, u"重复提交第三方"),
    "DONE": (2L, u"下分操作"),
    "NOTIFY": (3L, u"手动回调"),
    "REJECT": (4L, u"拒绝下分")
})


class WithdrawChannel(orm.Model):
    __tablename__ = "withdraw_channel"
    id = orm.Column(orm.BigInteger, primary_key=True)
    channel_type = orm.Column(orm.Integer)
    channel_name = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.Integer)
    appid = orm.Column(orm.VARCHAR)
    status = orm.Column(orm.SmallInteger)
    rate = orm.Column(orm.Float)
    max_amount = orm.Column(orm.SmallInteger)
    min_amount = orm.Column(orm.SmallInteger)
    start_hour = orm.Column(orm.SmallInteger)
    end_hour = orm.Column(orm.SmallInteger)
    deleted = orm.Column(orm.SmallInteger, default=0)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class WithdrawOrder(orm.Model):
    __tablename__ = 'withdraw_order'
    id = orm.Column(orm.BigInteger, primary_key=True)
    mch_id = orm.Column(orm.Integer)
    user_id = orm.Column(orm.BigInteger)
    pay_type = orm.Column(orm.VARCHAR)
    pay_account_bank = orm.Column(orm.VARCHAR)
    pay_account_bank_subbranch = orm.Column(orm.VARCHAR)
    pay_account_num = orm.Column(orm.VARCHAR)
    pay_account_username = orm.Column(orm.VARCHAR)
    trans_card_name = orm.Column(orm.VARCHAR)
    context = orm.Column(orm.TEXT)
    amount = orm.Column(orm.Integer)
    region = orm.Column(orm.VARCHAR)
    client_ip = orm.Column(orm.VARCHAR)
    sdk_version = orm.Column(orm.VARCHAR)
    out_trade_no = orm.Column(orm.VARCHAR)
    status = orm.Column(orm.VARCHAR)
    # 通知业务方
    notify_url = orm.Column(orm.VARCHAR)
    notify_status = orm.Column(orm.SmallInteger, default=0)
    notify_count = orm.Column(orm.SmallInteger, default=0)
    notify_at = orm.Column(orm.DATETIME)

    # 第三方通知支付的时间
    detail = orm.Column(orm.TEXT)
    admin = orm.Column(orm.TEXT)
    third_type = orm.Column(orm.SmallInteger)
    third_id = orm.Column(orm.VARCHAR)
    third_notify_at = orm.Column(orm.DATETIME)
    extra_info = orm.Column(orm.TEXT)

    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
