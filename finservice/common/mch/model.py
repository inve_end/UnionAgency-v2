# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

MCH_NOT_DELETED = 0
MCH_DELETED = 1

MCH_TYPE = Enum({
    "LOKI": (1L, u"幸运"),
    "WITCH": (2L, u"万壕"),
    "KS": (3L, u"凯萨"),
    "ZS": (5L, u"战神"),
    "DBL": (6L, u"大波罗"),
    "DG": (7L, u"Dagent"),
    "SP": (9L, u"SP"),
    "TT": (10L, u"泰坦"),
})


class MchAccount(orm.Model):
    """
    商户账号
    """
    __tablename__ = "mch_account"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    name = orm.Column(orm.VARCHAR)  # 商户名称
    password = orm.Column(orm.VARCHAR)  # 商户密码，MD5+salt
    api_key = orm.Column(orm.VARCHAR)  # 商户秘钥
    enabled = orm.Column(orm.SmallInteger)  # 商户秘钥
    deleted = orm.Column(orm.SmallInteger, default=0)
    third_min_amount = orm.Column(orm.BigInteger)  # 三方下分最小金额
    third_max_amount = orm.Column(orm.BigInteger)  # 三方下分最大金额
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    order_url = orm.Column(orm.VARCHAR)
