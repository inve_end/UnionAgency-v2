# -*- coding: utf-8 -*-
import json
from functools import wraps

import marshmallow
from django.http import HttpRequest, QueryDict

from common.utils.exceptions import ClientError


def validate_parameters(schema):
    def decorator(func):

        @wraps(func)
        def wrapper(*args, **kwargs):

            request = args[0]
            if not isinstance(request, HttpRequest):
                raise Exception("the first parameter must be request, "
                                "you must use @method_decorator(validate_parameters) if you use the class-based View.")

            content_type = request.META.get('CONTENT_TYPE')

            if request.method == 'GET':
                body = QueryDict(request.META['QUERY_STRING'])
                body = json.loads(body.get('p')) if body.get('p') else body
            else:
                body = request.body.decode('utf-8')
                if content_type.startswith('application/json'):
                    body = json.loads(body) if body else dict()
                elif content_type == 'application/x-www-form-urlencoded':
                    body = QueryDict(body)
                else:
                    raise ClientError('content-type must be application/json or application/x-www-form-urlencoded')
            try:
                cleaned_data = schema(unknown=marshmallow.EXCLUDE).load(body)
            except marshmallow.ValidationError as err:
                raise ClientError('params invalid, cause of : {}'.format(err.messages))

            return func(*args, cleaned_data=cleaned_data, **kwargs)

        return wrapper

    return decorator
