# -*- coding: utf-8 -*-
from gis.common import track_logging
import threading
import time

from .store import store_engine

logger = track_logging.getLogger(__name__)

# 空闲时线程休眠时间（秒）
second_of_wait_task = 1
# 任务处理器
handler_map = {}
# 结束任务信号通道
shutdown_signal = False
is_running = False


def register_handler(task_handler):
    handler_map[task_handler.get_biz_code()] = task_handler


def send_shutdown_signal():
    global shutdown_signal, is_running
    shutdown_signal = True
    is_running = False


class TaskProcessThread(threading.Thread):
    def run(self):
        logger.info('start the task processor.....')
        global is_running
        is_running = True
        while not shutdown_signal:
            undo_tasks = store_engine.get_undo_tasks()
            logger.debug('get undotask size: %s' % len(undo_tasks))

            if not undo_tasks:
                time.sleep(second_of_wait_task)
                continue

            for task in undo_tasks:
                try:
                    logger.info('begin process the task: %s' % task)
                    next_time = handler_map.get(task.biz_code).handle(task)
                    if next_time:
                        # 再次执行此任务
                        logger.info('retry the task: %s' % task)
                        store_engine.retry_task(task.biz_code, task.biz_num, next_time)
                    else:
                        # 标识此任务已完成
                        logger.info('finished the task: %s' % task)
                        store_engine.finished_task(task.biz_code, task.biz_num)
                except Exception:
                    logger.exception('fail process task: %s' % task)
        else:
            logger.info('the task executor had shutdown')


def run():
    if not is_running:
        for i in range(2):
            TaskProcessThread().start()
    else:
        logger.info('task is running, not allow start again')
