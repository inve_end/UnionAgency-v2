class BizErrorCode:
    def __init__(self, code, message):
        self.code = code
        self.message = message


# 预定义常见错误类型
ILLEGAL_PARAMETER = BizErrorCode(1, '参数错误')


class BizException(Exception):
    def __init__(self, error_code, message=None):
        assert isinstance(error_code, BizErrorCode)
        self.error_code = error_code
        _msg = error_code.message
        if message:
            _msg = '{} - {}'.format(error_code.message, message)
        self.detail_message = _msg
        super().__init__('code: {}, message: {}'.format(error_code.code, _msg))
