# -*- coding: utf-8 -*-
import logging
import random
import requests
import json
import time
import datetime

THIRD_API_KEY = 'a0aff92c7540914dc62222df31347b525d502097172888bb98475975'
THIRD_URL = 'http://103.230.240.228:9001/'

def tonglue_check_balance(bank_flag, card_number):
    try:
        post_data = {
            "apikey": THIRD_API_KEY,
            "bank_flag": bank_flag,
            "card_number": card_number,
        }

        response = requests.post(THIRD_URL + 'authority/system/api/query_bankcard/',
            headers={'Content-type': 'application/json', 'Accept': 'text/plain'},
            data=json.dumps(post_data), timeout=5)
        response = json.loads(response.content)
        print post_data
        print response
        if response[u'success']:
            return response
    except Exception, e:
        print e

if __name__ == "__main__":
    tonglue_check_balance('ABC', '6217920606688120')
