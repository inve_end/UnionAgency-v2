# -*- coding: utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import hashlib
import json

import requests


def generate_sign(parameter, key='b12f2adf9aca408983f6a5c5efdf30e7'):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def create_charge():
    parameter_dict = {"amount": "1.2", "companyOrderNo": "Testing_296289", "accountNumber": " 6215581210001681501",
                      "playerId": "9999991002", "company": "Testing",
                      "notifyUrl": "http://api.maiunion.net:9003/fin/api/notify/hydra/Testing",
                      "sign": "D8B445D02B92410076A39E94B210FB27", "bank": "CCB", "accountName": "\u5b63\u5de7\u8d5f"}
    j = json.dumps(parameter_dict)
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    res = requests.post('http://103.71.51.51:5006/api/Orders', data=j, headers=headers, timeout=5).text
    return res


if __name__ == "__main__":
    print create_charge()
