# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
import sys
import json
import tablib
import logging
import time
import datetime

# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'

import requests
from common.mch import handler as mch_handler


CHARGE_URL = 'http://127.0.0.1:8989/api/charge/create/'


def test_charge(**kwargs):
    sdk_version = kwargs.get('sdk_version')
    service = kwargs.get('service')
    mch_id = kwargs.get('mch_id')
    out_trade_no = kwargs.get('out_trade_no')
    body = kwargs.get('body')
    total_fee = kwargs.get('total_fee')
    mch_create_ip = kwargs.get('mch_create_ip')
    notify_url = kwargs.get('notify_url')
    sign = mch_handler.generate_sign(mch_id, kwargs)
    print sign
    kwargs.update(sign=sign)
    res = requests.post(CHARGE_URL, data=kwargs, timeout=3)
    print res.text


if __name__ == "__main__":
    test_charge(sdk_version='android_v_1_0', service='alipay', mch_id='6001000',
        out_trade_no=int(time.time()), body=u'实惠网盘', total_fee=11, mch_create_ip='127.0.0.1',
        notify_url='http://219.135.56.195/api/v1/justpay/notify/')
