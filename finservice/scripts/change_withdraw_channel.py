# -*- coding: utf-8 -*-
from __future__ import absolute_import

import logging
import os
import sys

# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'

from common.withdraw.model import WithdrawOrder, WITHDRAW_ORDER_STATUS
from common.withdraw.handler import process_withdraw

_LOGGER = logging.getLogger('worker')

error_order_no = [1666070447362549760, 1666070162983492608, 1666070149131829248, 1666069635968149504,
                  1666069558835965952
    , 1666066742010567680
    , 1666066762014208000
    , 1666065887230226432
    , 1666065765996994560
    , 1666065603971131392
    , 1666065591249798144
    , 1666065417915929600
    , 1666065267317278720
    , 1666064908215782400
    , 1666064748472745984
    , 1666064074012484608
    , 1666063852589491200
    , 1666063500150886400
    , 1666063029648854016
    , 1666061917672833024
    , 1666061838447581184
    , 1666061760543775744
    , 1666061619631859712
    , 1666061447061919744
    , 1666061279348966400
    , 1666061146489106432
    , 1666060759875998720
    , 1666060710516369408
    , 1666058411452894208
    , 1666058545183850496
    , 1666057425759165440
    , 1666057242811626496
    , 1666056263354604544
    , 1666056685114971136
    , 1666056675737667584
    , 1666056665752267776
    , 1666056307043859456
    , 1666056102476107776
    , 1666055853025068032
    , 1666055887190600704
    , 1666055638131791872
    , 1666055413981337600
    , 1666055014086515712
    , 1666054891213683712
    , 1666054631842735104
    , 1666054533562853376
    , 1666054424254070784
    , 1666054188652711936
    , 1666054186663699456
    , 1666054048757397504, 1666053963491993600
    , 1666053894373004288
    , 1666053743857137664, 1666053742120852480, 1666053659857887232, 1666053624774709248]


def handle(mch_id, order_nos, status):
    print 'Run fix withdraw'
    withdraw_orders = WithdrawOrder.query.filter(WithdrawOrder.mch_id == mch_id).filter(
        WithdrawOrder.out_trade_no.in_(order_nos)).filter(
        WithdrawOrder.status == status).all()
    for order in withdraw_orders:
        process_withdraw(order)
        print 'fix orders %s', order.out_trade_no


if __name__ == "__main__":
    print 'Run fix withdraw'
    handle(3, error_order_no, WITHDRAW_ORDER_STATUS.THIRD_FAIL)
    print 'end fix withdraw'
