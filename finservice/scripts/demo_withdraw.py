# -*- coding: utf-8 -*-
"""
自建银行卡代理下分
"""
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import hashlib
import json
import logging
import random

import requests
from datetime import datetime

# MCH_ID = 1
# API_KEY = '2f6a6bf4f64a4a37bc415e916894d399'
# MCH_ID = 8
# API_KEY = 'c46444d559dd4892adcc6a8a9a5f9403'
# MCH_ID = 7
# API_KEY = '1606a0d5919948e8900e2592a2bb9245'
# MCH_ID = 6
# API_KEY = 'e88099a326584f8cbdbc2768d72e3f3b'
# MCH_ID = 6
# API_KEY = 'e88099a326584f8cbdbc2768d72e3f3b'
# MCH_ID = 6
# API_KEY = 'e88099a326584f8cbdbc2768d72e3f3b'
# MCH_ID = 1
# API_KEY = '2f6a6bf4f64a4a37bc415e916894d399'
# MCH_ID = 7
# API_KEY = '87918f5eece349e38c4cc0e01aad9009'
MCH_ID = 2
API_KEY = 'b12f2adf9aca408983f6a5c5efdf30e7'
# MCH_ID = 3
# API_KEY = '31c44e0acbae4f238f8359eabce9ffbc'
# MCH_ID = 6
# API_KEY = '4b2556a3c0f24e08850329d71e425983'
CHARGE_URL = 'http://test.maiunion.net:8084/api/v2/withdraw/create/'  # 提交下分订单
QUERY_URL = 'http://test.maiunion.net:8084/api/v2/withdraw/query/'  # 查询订单状态
# CHARGE_URL = 'http://api.maiunion.net:9003/api/v2/withdraw/create/'  # 提交下分订单
# QUERY_URL = 'http://api.maiunion.net:9003/api/v2/withdraw/query/'  # 查询订单状态

'''
订单状态如下：
WITHDRAW_ORDER_STATUS = Enum({
    "WAIT": (0L, "待下分"), # 已经收到，但未处理
    "LOCKED": (1L, "已锁定"), # 已经收到，处理中
    "DONE": (2L, "已下分"), # 已处理，应该已到账
    "REJECTED": (3L, "拒绝下分"), # 已拒绝，请给用户返款，原因见回调
})

回调通知示例：
{
    'trade_no': 735L, # 商户订单号
    'mch_id': 3,
    'pay_result': 2, # 订单状态
    'total_fee': 100.0,
    'reason': u'success', # 原因
    'sign': 'CDEFA02847373BB26A78A6A1C54E8BED'
}
'''


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    print s
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


# CCB002  贾志强  6217000140028203907
def create_charge(pay_amount):
    parameter_dict = {
        'sdk_version': u'ios_5.1',
        'mch_id': MCH_ID,
        'user_id': u'123456',  # 用户ID
        'out_trade_no': random.randint(100000000000, 200000000000),
        'body': u'提现测试',
        'pay_type': u'bankcard',  # 后续可能开放wechat, alipay
        'pay_account_num': u'6217000170011621054',  # 提现卡号
        'pay_account_username': u'田文博',  # 卡主姓名
        'pay_account_bank': u'中国邮政储蓄银行',  # 银行名称
        'pay_account_bank_subbranch': u'河北石家庄联盟路支行',  # 支行名称
        'amount': pay_amount,  # 金额50-50000
        'notify_url': u'http://test.maiunion.net:8084/notify',
        'region': u'香港',
        'client_ip': u'103.111.11.1',
    }
    print parameter_dict['out_trade_no']
    parameter_dict['sign'] = generate_sign(parameter_dict, API_KEY)
    res = requests.post(CHARGE_URL, data=parameter_dict, timeout=5).text
    return res


def query(out_trade_no):
    parameter_dict = {
        'mch_id': MCH_ID,
        'out_trade_no': out_trade_no,
        'random_num': 4444,
    }
    parameter_dict['sign'] = generate_sign(parameter_dict, API_KEY)
    res = requests.post(QUERY_URL, data=parameter_dict, timeout=10).text
    return res


def generate_hydra_sign(d, key):
    s = d['company'] + d['companyOrderNo'] + d['playerId'] + d['Bank'] + d['accountName'] + d['accountNumber'] + \
        d['amount'] + d['notifyUrl'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    print s
    sign = m.hexdigest().upper()
    return sign


if __name__ == "__main__":
    print create_charge(99.9)
    # print query(157756750382)
    # "sign": "CD229F545E0C60C3FF422377A45745FB",
    # d = {"amount": "50", "companyOrderNo": "loki_422336", "accountNumber": "6236683260002889440", "playerId": "1243571",
    #  "company": "Durotar", "notifyUrl": "http://api.maiunion.net:9003/fin/api/notify/hydra_withdraw/Durotar",
    #   "Bank": "CCB", "accountName": "\u5510\u5b97\u7fa1"}
    # print generate_hydra_sign(d, 'HyDra_Unionagency1.0@prod_server')
