# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from console.views import bankcard as bankcard_view
from console.views import chase as chase_view
from console.views import data as data_view
from console.views import manual_trans as manual_trans_view
from console.views import mch as mch_view
from console.views import recharge as recharge_view
from console.views import risky as risk_view
from console.views import user as user_view
from console.views import withdraw as withdraw_view
from console.views import withdraw_channel as withdraw_channel_view
from console.views.order_inquiry import OrderInquiryView, SingleOrderInquiryView

urlpatterns = patterns(
    '',
    # user
    url(r'^user/login/?$', user_view.login),  # 用户登陆 101
    url(r'^user/logout/?$', user_view.logout),  # 用户登出 102
    url(r'^user/?$', user_view.UserView.as_view()),  # 注册用户 103
    url(r'^user/(?P<user_id>\d+)/?$', user_view.SingleUserView.as_view()),  # 获取/修改用户详情[本人] 104
    url(r'^user/info/?$', user_view.modify),  # 获取/修改用户详情[本人] 104

    # user management
    url(r'^manage/get_user_list/?$', user_view.get_user_list),  # 获取用户列表 201
    url(r'^manage/update_user_info/(?P<user_id>\d+)/?$', user_view.update_user_info),  # 修改用户信息[他人] 202
    url(r'^manage/get_user_info/(?P<user_id>\d+)/?$', user_view.get_user_info),  # 获取用户信息[他人] 203
    url(r'^manage/delete_user/(?P<user_id>\d+)/?$', user_view.delete_user),  # 删除用户 204
    url(r'^manage/get_role_list/?$', user_view.get_role_list),  # 获取角色列表 205
    url(r'^manage/update_role_info/(?P<role_id>\d+)/?$', user_view.update_role_info),  # 更新角色 206
    url(r'^manage/create_role/?$', user_view.create_role),  # 创建角色 207
    url(r'^manage/get_role/(?P<role_id>\d+)/?$', user_view.get_role),  # 获取角色详情 208
    url(r'^manage/delete_role/(?P<role_id>\d+)/?$', user_view.delete_role),  # 删除角色 209
    url(r'^manage/get_perm_list/?$', user_view.list_perm),  # 获取权限列表 210
    url(r'^manage/get_perm_mod/?$', user_view.list_perm_mod),  # 获取权限大模块列表 211
    url(r'^manage/get_perm_tree/?$', user_view.list_perm_tree),  # 获取权限列表[树结构] 212
    url(r'^manage/record/?$', user_view.list_records),  # 查看操作记录 213

    # mch
    url(r'^mch/list/?$', mch_view.MchView.as_view()),  # 获取商户列表 301
    url(r'^mch/create/?$', mch_view.MchView.as_view()),  # 创建商户 302
    url(r'^mch/detail/(?P<mch_id>\d+)/?$', mch_view.SingleMchView.as_view()),  # 获取商户详情 303
    url(r'^mch/update_status/(?P<mch_id>\d+)/?$', mch_view.update_status),  # 禁用商户 304
    url(r'^mch/update_info/(?P<mch_id>\d+)/?$', mch_view.SingleMchView.as_view()),  # 修改商户信息 305
    url(r'^mch/delete/(?P<mch_id>\d+)/?$', mch_view.SingleMchView.as_view()),  # 删除商户 306

    # withdraw_channel
    url(r'^withdraw_channel/list/?$', withdraw_channel_view.WithdrawChannelView.as_view()),  # 获取提现通道列表 401
    url(r'^withdraw_channel/create/?$', withdraw_channel_view.WithdrawChannelView.as_view()),  # 创建提现通道 402
    # 获取提现通道详情 403
    url(r'^withdraw_channel/detail/(?P<channel_id>\d+)/?$', withdraw_channel_view.SingleWithdrawChannelView.as_view()),
    # 修改提现通道信息 404
    url(r'^withdraw_channel/update/(?P<channel_id>\d+)/?$', withdraw_channel_view.SingleWithdrawChannelView.as_view()),
    # 删除提现通道信息 405
    url(r'^withdraw_channel/delete/(?P<channel_id>\d+)/?$', withdraw_channel_view.SingleWithdrawChannelView.as_view()),

    # risk check
    url(r'^risk/card/list/?$', risk_view.RiskCardView.as_view()),  # 获取黑名单列表 501
    url(r'^risk/card/create/?$', risk_view.RiskCardView.as_view()),  # 添加黑名单 502
    url(r'^risk/card/update/(?P<card_id>\d+)/?$', risk_view.SingleRiskCardView.as_view()),  # 修改黑名单 503
    url(r'^risk/card/delete/(?P<card_id>\d+)/?$', risk_view.delete_black_card),  # 删除黑名单 504
    url(r'^risk/card/detail/(?P<card_id>\d+)/?$', risk_view.SingleRiskCardView.as_view()),  # 获取黑名单详情 505
    url(r'^risk/user/detail/(?P<user_id>\d+)/?$', risk_view.get_user_info),  # 获取玩家详情 506
    url(r'^risk/user/list/?$', risk_view.get_user_list),  # 获取玩家行为列表 507
    url(r'^risk/user/update/(?P<user_id>\d+)/?$', risk_view.update_user_status),  # 修改玩家状态 508
    url(r'^risk/user/iplist/(?P<user_id>\d+)/?$', risk_view.get_user_ip),  # 获取玩家历史IP 509

    # bankcard manage
    url(r'^bankcard/list/?$', bankcard_view.list_bankcard),  # 获取银行卡列表 701
    url(r'^bankcard/list/chargeable/(?P<mch_id>\d+)/?$', bankcard_view.list_chargeable_bankcard),  # 获取后台可上分的银行
    url(r'^bankcard/create/?$', bankcard_view.create_bankcard),  # 创建银行卡列表 702
    url(r'^bankcard/detail/(?P<card_id>\d+)/?$', bankcard_view.get_bankcard_info),  # 获取银行卡详情 703
    url(r'^bankcard/update/(?P<card_id>\d+)/?$', bankcard_view.update_bankcard_info),  # 更新银行卡信息 704
    url(r'^bankcard/update_status/(?P<card_id>\d+)/?$', bankcard_view.update_bankcard_status),  # 更新银行卡状态 705
    url(r'^bankcard/password/(?P<card_id>\d+)/?$', bankcard_view.get_bankcard_password),  # 获取银行卡密码 706
    url(r'^bankcard/update_password/(?P<card_id>\d+)/?$', bankcard_view.update_bankcard_password),  # 更新银行卡密码 707

    url(r'^bankcard/get_bankcard_stars/(?P<card_id>\d+)/?$', bankcard_view.get_bankcard_stars),  # 获取派卡信息 708
    url(r'^bankcard/update_bankcard_stars/(?P<card_id>\d+)/?$', bankcard_view.update_bankcard_stars),  # 更新派卡信息 709

    url(r'^bankcard/list_peroid/?$', bankcard_view.list_bankcard_peroid),  # 银行卡生命周期列表 712
    url(r'^bankcard/create_peroid/?$', bankcard_view.create_peroid),  # 创建银行卡生命周期 713
    url(r'^bankcard/peroid_detail/(?P<peroid_id>\d+)/?$', bankcard_view.get_bankcard_peroid),  # 银行卡生命周期详情 714
    url(r'^bankcard/update_peroid/(?P<peroid_id>\d+)/?$', bankcard_view.update_bankcard_peroid),  # 更新银行卡生命周期 715
    url(r'^bankcard/delete_peroid/(?P<peroid_id>\d+)/?$', bankcard_view.delete_bankcard_peroid),  # 删除银行卡生命周期 716
    url(r'^bankcard/delete/(?P<card_id>\d+)/?$', bankcard_view.delete_bankcard),  # 删除银行卡 717
    url(r'^bankcard/report/(?P<card_id>\d+)/?$', bankcard_view.get_bankcard_report),  # 获取银行卡统计数据 718

    url(r'^bankcard/trans/list/?$', manual_trans_view.list_manual_trans),  # 获取人工填报列表 730
    url(r'^bankcard/trans/create/?$', manual_trans_view.create_manual_trans),  # 创建人工填报 731
    # url(r'^bankcard/trans/outer/create/?$', manual_trans_view.create_manual_outer_trans), # 创建外转人工填报 732 (已取消)
    url(r'^bankcard/trans/update/(?P<trans_id>\d+)/?$', manual_trans_view.update_manual_trans),  # 更新人工填报备注 733
    url(r'^bankcard/trans/detail/(?P<trans_id>\d+)/?$', manual_trans_view.get_manual_trans_detail),  # 获取人工填报详情 734
    url(r'^bankcard/batch_create/?$', bankcard_view.batch_create_bankcard),  # 上传银行卡列表 735

    # recharge_order
    url(r'^recharge_order/list/?$', recharge_view.RechargeOrderView.as_view()),  # 上分订单列表 801
    url(r'^recharge_order/detail/(?P<order_id>\d+)/?$', recharge_view.SingleRechargeOrderView.as_view()),  # 上分订单详情 802
    url(r'^recharge_order/approve/(?P<order_id>\d+)/?$', recharge_view.approve_recharge_order),  # 批准上分 803
    url(r'^recharge_order/reject/(?P<order_id>\d+)/?$', recharge_view.reject_recharge_order),  # 拒绝上分 804
    url(r'^recharge_order/notify/(?P<order_id>\d+)/?$', recharge_view.notify_recharge_order),  # 上分通知 805
    url(r'^recharge_order/resend/(?P<order_id>\d+)/?$', recharge_view.resend_recharge_order),  # 重新向第三方上分 821

    # withdraw_order
    url(r'^withdraw_order/list/?$', withdraw_view.WithdrawOrderView.as_view()),  # 下分订单列表 806
    url(r'^withdraw_order/detail/(?P<order_id>\d+)/?$', withdraw_view.SingleWithdrawOrderView.as_view()),  # 下分订单详情 807
    url(r'^withdraw_order/lock/(?P<order_id>\d+)/?$', withdraw_view.lock_withdraw_order),  # 锁定下分订单 808
    url(r'^withdraw_order/approve/(?P<order_id>\d+)/?$', withdraw_view.approve_withdraw_order),  # 批准下分 809
    url(r'^withdraw_order/reject/(?P<order_id>\d+)/?$', withdraw_view.reject_withdraw_order),  # 拒绝下分 810
    url(r'^withdraw_order/notify/(?P<order_id>\d+)/?$', withdraw_view.notify_withdraw_order),  # 下分通知 811
    url(r'^withdraw_order/resend/(?P<order_id>\d+)/?$', withdraw_view.resend_withdraw_order),  # 重新向第三方下分 819
    url(r'^withdraw_order/batch_operator/?$', withdraw_view.batch_withdraw_order),  # 批量操作 820
    url(r'^withdraw_order/refresh_payorder/(?P<order_id>\d+)/?$', withdraw_view.refresh_withdraw_order),  # 批量操作 820

    # 同步第三方成功上分 812
    url(r'^withdraw_order/approve_third/(?P<order_id>\d+)/?$', withdraw_view.approve_third_withdraw_order),
    url(r'^chase_order/create/?$', chase_view.create_order),  # 创建追分订单 813
    url(r'^chase_order/list/?$', chase_view.get_order_list),  # 获取追分订单列表 814

    # test_order
    url(r'^recharge_order/create/?$', recharge_view.create_test_order),  # 创建测试充值订单 815
    # url(r'^withdraw_order/create/?$', withdraw_view.create_test_order), # 创建测试提现订单 816(太危险，未实现)
    url(r'^recharge_order/export/?$', recharge_view.export_recharge_orders),  # 导出上分订单 817
    url(r'^withdraw_order/export/?$', withdraw_view.export_withdraw_orders),  # 导出下分订单 818

    # data analysis
    url(r'^data/withdraw/hourly/?$', data_view.get_withdraw_hourly),  # 下分数据统计(每时) 901
    url(r'^data/withdraw/daily/?$', data_view.get_withdraw_daily),  # 下分数据统计(每天) 902
    url(r'^data/recharge/hourly/?$', data_view.get_recharge_hourly),  # 上分数据统计(每时) 903
    url(r'^data/recharge/daily/?$', data_view.get_recharge_daily),  # 上分数据统计(每天) 904
    url(r'^data/recharge_effi/hourly/?$', data_view.get_recharge_effi_hourly),  # 上分效率统计(每时) 905
    url(r'^data/recharge_effi/daily/?$', data_view.get_recharge_effi_daily),  # 上分效率统计(每天) 906
    url(r'^data/withdraw_effi/hourly/?$', data_view.get_withdraw_effi_hourly),  # 下分效率统计(每时) 907
    url(r'^data/withdraw_effi/daily/?$', data_view.get_withdraw_effi_daily),  # 下分效率统计(每天) 908

    # 代理充值下单
    url(r'^recharge_order/make_order/?$', recharge_view.make_order),  # 910

    # order inquiry
    url(r'^orderinquiry/list/?$', OrderInquiryView.as_view()),  # 910
    url(r'^orderinquiry/(?P<order_inquiry_id>\d+)/?$', SingleOrderInquiryView.as_view())
)
