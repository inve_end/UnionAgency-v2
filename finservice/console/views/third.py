# -*- coding: utf-8 -*-
import json

import requests
from django.conf import settings
from django.views.decorators.http import require_GET

from common.utils import track_logging
from common.utils.decorator import response_wrapper

_LOGGER = track_logging.getLogger(__name__)


@require_GET
@response_wrapper
def get_query_bankcard(request):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    post_data = {
        "apikey": "a0aff92c7540914dc62222df31347b525d502097172888bb98475975",
        "bank_flag": "CMB",
        "card_number": "6214837911625998"
    }
    proxies = settings.THIRD_PROXY
    response = requests.post(settings.THIRD_URL + 'authority/system/api/query_bankcard/',
                             data=json.dumps(post_data), proxies=proxies, headers=headers, timeout=5)
    response = json.loads(response.content)
    return response
