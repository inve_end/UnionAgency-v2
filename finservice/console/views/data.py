# -*- coding: utf-8 -*-

from common.data import handler as data_handler
from common.data.handler import get_date_range
from common.utils import track_logging
from common.utils.api import token_required
from common.utils.decorator import response_wrapper

_LOGGER = track_logging.getLogger(__name__)


@token_required
@response_wrapper
def get_withdraw_daily(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    data = data_handler.get_withdraw_data('daily', mch_id, started_at, ended_at)

    return data


@token_required
@response_wrapper
def get_withdraw_hourly(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    data = data_handler.get_withdraw_data('hourly', mch_id, started_at, ended_at)

    return data


@token_required
@response_wrapper
def get_recharge_hourly(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    pay_type = query_dct.get('pay_type')
    data = data_handler.get_recharge_data('hourly', started_at, ended_at, mch_id, pay_type)

    return data


@token_required
@response_wrapper
def get_recharge_daily(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    pay_type = query_dct.get('pay_type')
    data = data_handler.get_recharge_data('daily', started_at, ended_at, mch_id, pay_type)

    return data


@token_required
@response_wrapper
def get_withdraw_effi_hourly(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    data = data_handler.get_withdraw_effi_data('hourly', mch_id, started_at, ended_at)

    return data


@token_required
@response_wrapper
def get_withdraw_effi_daily(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    data = data_handler.get_withdraw_effi_data('daily', mch_id, started_at, ended_at)

    return data


@token_required
@response_wrapper
def get_recharge_effi_hourly(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    pay_type = query_dct.get('pay_type')
    data = data_handler.get_recharge_effi_data('hourly', mch_id, started_at, ended_at, pay_type)

    return data


@token_required
@response_wrapper
def get_recharge_effi_daily(req):
    query_dct = req.GET.dict()
    started_at, ended_at = get_date_range(query_dct)
    mch_id = query_dct.get('mch_id')
    pay_type = query_dct.get('pay_type')
    data = data_handler.get_recharge_effi_data('daily', mch_id, started_at, ended_at, pay_type)

    return data
